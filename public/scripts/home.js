var $spaces = 0;
var $other_key_than_period = false;
$(document).ready(function(){
	$('.pile_title').click(function(){
		$(this).parent().find(".notebooks").slideToggle();
	});
	$('.notebook_title').click(function(){
		$('.notebook_title').removeClass("selected");
		$(this).addClass("selected");
		$path = $(this).parent().find(".path").val();
        load_notes_list($path)
	});

});
function load_notes_list($path){
    $.ajax({
        url:"/index.php/get_notes_list",
        data: {path:$path},
        method: "POST",
        success: function($data){
            $('#list').html($data);
            init_notes();
        }
    });
}
function init_notes(){
    $('div.new_note').click(function(){
        $(this).hide();
        $('.new_note_inputs').show();
        $('.new_note_inputs .new_note').focus();
    })
    $('#create_new_note').click(function(){
        if ($('.new_note_inputs .new_note').val().trim() != ''){
            const fetchHeaders = { 'Content-type' : 'application/x-www-form-urlencoded' }
            let fileName = $('.new_note_inputs .new_note').val();
		    let filePath = $('#current_path').val();
            fetch('/index.php/create_file',{
                'method' : 'POST',
                'headers' : fetchHeaders,
                'body' : `file_name=${fileName}&file_path=${filePath}`
            })
            .then( response => {
                return response.text()
            })
            .then( response => {
                load_notes_list(filePath);
            })
        }
    })
	$('.note_link').click(function(){
		$('.note_link').removeClass('selected');
		$(this).addClass('selected');
		$path = $(this).find(".path").val();
		$name = $(this).find(".title").val();
		$.ajax({
			url:"/index.php/get_note",
			data: {path:$path,name:$name},
			method: "POST",
			success: function($data){
				$('#note').html($data);
				$('#editor1').ckeditor(function(){
						init_note();
				},{
			        toolbar : 'Standard',
			        uiColor : '#ffdddd',
			        width	: '58vw',
			        height	: '74vh'
			  });
			}
		});
	});
	function init_note(){
        $('.cke_wysiwyg_frame').contents().find('body').keypress(function($event){
            if($event.which == 46 || $event.which == 44){
                if($other_key_than_period){
                    save();
                }
            }
            else{
                $other_key_than_period = true;
                if ($event.which == 32){
                    $spaces++;
                    if ($spaces == 5){
                        save();
                    }
                }
            }
        });
        $('.save').click(function(){
            save();
        });
	}
	function save(){
		$new_name	= $('.title-holder input').val();
		$date 		= $('.date-holder span').html();
		$path 		= $('#note_path').val();
		$name 		= $('#note_name').val();
		$content 	= $('#editor1').val();
		$('.save_box input').hide();
		$('.save_box img').show();
		$.ajax({
				url:"/index.php/save_note",
				data: {path:$path,name:$name,date:$date,new_name:$new_name,content:$content},
				method: "POST",
				success: function($response){
						if ($response != 't'){
								$('.cke_wysiwyg_frame').contents().find('body').append("ERROR AL INTENTAR GUARDAR");
								$('.cke_wysiwyg_frame').contents().find('body').css('background-color',"#ff8888");
						}
						else{
							$('.save_box img').hide();
							$('.save_box input').show();
						}
				}
		});
		$other_key_than_period = false;
		$spaces = 0;
	}
}
