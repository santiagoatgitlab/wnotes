<input type="hidden" id="current_path" value="<?php echo $path; ?>">
<div class="new_note">
   <span>Nueva nota</span>
</div>
<div class="new_note_inputs">
    <input type="text" class="new_note">
    <input type="button" value="+" id="create_new_note">
</div>
<?php
for($i = 0; $i < count($notes); $i++){
	?>
	<div class="note_link">
		<span class="name">
		<?php echo preg_replace('/^|.html$/', '', $notes[$i]) . "<br>"; ?>
		</span>
		<input type="hidden" class="path" value="<?php echo $path; ?>" >
		<input type="hidden" class="title" value="<?php echo urlencode($notes[$i]); ?>" >
	</div>
<?php } ?>
