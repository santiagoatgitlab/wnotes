<html lang="en">
<head>
	<meta charset="utf-8">
	<title>W notes</title>
	 <link rel="stylesheet" type="text/css" href="/styles/main.css">
	 <script src="/scripts/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="/scripts/home.js"></script>
	<script type="text/javascript" src="/plugins/ckeditor_basic/ckeditor.js"></script>
	<script type="text/javascript" src="/plugins/ckeditor_basic/adapters/jquery.js"></script>
	</script>
</head>
<body>

<div id="container">

<div id="index" class="col">
	<?php for($i = 0; $i < count($piles); $i++){ ?>
			<div class="pile">
				<div class="pile_title">
					<?php echo $piles[$i]['name']; ?>
				</div>
				<div class="notebooks">
					<?php for ($j = 0; $j < count($piles[$i]['notebooks']); $j++ ) { ?>
					<div class="notebook">
						<div class="notebook_title">
							<?php echo $piles[$i]['notebooks'][$j]["name"]; ?>
						</div>
						<input type="hidden" class="path" value="<?php echo $piles[$i]['notebooks'][$j]["path"]; ?>">
					</div>
					<?php } ?>
				</div>
			</div>
	<?php } ?>
</div>
<div id="list" class="col">
	Ninguna libreta seleccionada
</div>
<div id="note" class="col">
	Ninguna nota seleccionada
</div>

</div>

</body>
</html>
