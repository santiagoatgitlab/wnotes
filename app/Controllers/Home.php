<?php

namespace App\Controllers;

class Home extends BaseController {

	public function index()
	{
		$piles_dirs = scandir(NOTES_ROOT_PATH);
		for($i = 0; $i < count($piles_dirs); $i++){
			if (is_dir(NOTES_ROOT_PATH.$piles_dirs[$i]) && substr($piles_dirs[$i], 0, 1) != '.'){
				$pile['name'] = $piles_dirs[$i];
				$pile['notebooks'] = array();
				$notebook_dirs = scandir(NOTES_ROOT_PATH.$piles_dirs[$i]);
				for($j = 0; $j < count($notebook_dirs); $j++){
					if (is_dir(NOTES_ROOT_PATH.$piles_dirs[$i]."/".$notebook_dirs[$j]) && substr($notebook_dirs[$j], 0, 1) != '.'){
						$notebook["path"] = urlencode($piles_dirs[$i]."/".$notebook_dirs[$j]."/");
						$notebook["name"] = $notebook_dirs[$j];
						$pile['notebooks'][] = $notebook;
					}
				}
				$piles[] = $pile;
			}
		}
		$data = array('piles' => $piles);
		echo view('home',$data);
	}
}
