<?php

class Edit extends BaseController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$root_dir = scandir(NOTES_ROOT_PATH);
		foreach ($root_dir as $key_pile => $pile) {
			if (is_dir(NOTES_ROOT_PATH.$pile) && substr($pile, 0, 1) != '.'){
				$pile_dir = scandir(NOTES_ROOT_PATH.$pile);
				foreach ($pile_dir as $key_notebook => $notebook) {
					if (is_dir(NOTES_ROOT_PATH.$pile.'/'.$notebook) && substr($notebook, 0, 1) != '.'){
						$notebook_dir = scandir(NOTES_ROOT_PATH.$pile.'/'.$notebook);
						foreach ($notebook_dir as $key_note => $note_name) {

							if (!is_dir(NOTES_ROOT_PATH.$pile.'/'.$notebook.'/'.$note_name) && substr($note_name, 0, 1) != '.' ){
								echo $note_name;
								$note 		= file(NOTES_ROOT_PATH.$pile.'/'.$notebook.'/'.$note_name);
								$name 		= substr($note[0],4,strlen(trim($note[0]))-9);
								$datetime 	= trim(substr($note[1],strpos($note[1],"<i>")+3,16),'<');
								$date 		= explode(" ", $datetime)[0];
								$time 		= explode(" ", $datetime)[1];

								$date_splitted	= explode("/", $date);
								$date 			= $date_splitted[2] . '/' . $date_splitted[1] . '/' . $date_splitted[0];
								if (strlen($time) < 5){
									$time = '0'.$time;
								}
								$note[0] = $name . "\n";
								$note[1] = $date . ' ' . $time . "\n";
								$note[count($note)-1] = "</div>";
								file_put_contents(NOTES_ROOT_PATH.$pile.'/'.$notebook.'/'.$note_name, $note);
							}
						}
					}
				}
			}
		}

	}
}
