<?php

namespace App\Controllers;

class Ajax extends BaseController {

	public function get_notes_list()
	{
		$notes = scandir(NOTES_ROOT_PATH.urldecode($_POST['path']));
		if (count($notes)>2){
			for ($i=2; $i < count($notes); $i++) {
				if (!is_dir(NOTES_ROOT_PATH.urldecode($_POST['path'].$notes[$i]))){
						$lines = file(NOTES_ROOT_PATH.urldecode($_POST['path'].$notes[$i]));
						$date = $lines[1];
						$splitted_date = explode('/',$date);
						if (count($splitted_date)>2){
							$notes_by_date[$date][] = $notes[$i];
						}
				}
			}
			krsort($notes_by_date);
			$notes = array();
			foreach ($notes_by_date as $date_notes) {
				foreach ($date_notes as $note) {
					$notes[] = $note;
				}
			}
			$data = array('notes'=>$notes,'path'=>$_POST['path']);
		}
		else{
			$data = array('notes'=>array(),'path'=>$_POST['path']);
		}

		echo view('notes_list',$data);
	}
	public function get_note(){

		$note_lines = file(NOTES_ROOT_PATH.urldecode($_POST['path']).urldecode($_POST['name']));

		$data['title'] 	= trim(urldecode($_POST['name']),".html");
		$data['date']	= trim($note_lines[1]);
		$data['path'] = $_POST['path'];
		$data['name'] = $_POST['name'];
		$data['content'] = '';
		for ($i=2; $i < count($note_lines); $i++) {
			$data['content'] .= $note_lines[$i];
		}

		echo view('note',$data);
	}
	public function save_note(){

		$new_text = $_POST["new_name"] . "\n";
		$new_text .= $_POST["date"] . "\n";
		$new_text .= $_POST["content"] . "\n";

		if (file_put_contents(NOTES_ROOT_PATH.urldecode($_POST['path'].urldecode($_POST['name'])),$new_text)){
			echo 't';
		}
		else{
			echo 'f';
		}

		if ($_POST['name'] != $_POST['new_name'].".html"){

				rename(NOTES_ROOT_PATH.urldecode($_POST['path'].urldecode($_POST['name'])),
							NOTES_ROOT_PATH.urldecode($_POST['path'].urldecode($_POST['new_name']).".html")
						);
		}

	}

    public function create_file(){

		$new_text   = $_POST["file_name"] . "\n";
		$new_text  .= date('Y/m/d h:i')   . "\n";
        //echo $_POST['file_path'] . $_POST['file_name'];
        $fullPath = NOTES_ROOT_PATH.$_POST['file_path'].$_POST['file_name'].'.html';
        $result = false;
        if (!file_exists($fullPath)){
            $result = file_put_contents($fullPath,$new_text);
        }
        else {
            echo "file already exists";
        }
        if ($result){
            echo 'file created at ' . $fullPath;
        }

    }
}
